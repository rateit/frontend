import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

import authStore from './store/modules/auth'

import SignIn from './views/auth/SignIn'
import SignUp from './views/auth/SignUp'

import Dashboard from './views/dashboard/Dashboard'
import SearchResult from './views/search/SearchResult'

import Ranking from './views/shared/Ranking'

import User from './views/user/User'
import UserInfo from './views/user/UserInfo'
import Owned from './views/user/Owned'
import Rated from './views/user/Rated'
import Friendslist from './views/user/Friendslist'
import Wishlist from './views/user/Wishlist'
import Settings from './views/user/Settings'
import Notification from './views/user/Notification'
import Message from './views/messages/Message'

import AdminDashboard from './views/admin/AdminDashboard'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            component: Home,
            name: 'home',
        },
        {
            path: '/signin',
            component: SignIn,
            name: 'signin',
        },
        {
            path: '/signup',
            component: SignUp,
            name: 'signup'
        },
        {
            path: '/dashboard',
            component: Dashboard,
            name: 'dashboard',
            beforeEnter (to, from, next) {
                if(localStorage.getItem('token')) {
                    next()
                } else {
                    next('/signin')
                }
            },
        },
        {
            path: '/searchResult',
            component: SearchResult,
            name: 'searchResult'
        },
        {
            path: '/notification',
            component: Notification,
            name: 'notification'
        },
        {
            path: '/user/:id',
            name: 'user',
            component: User,
            children: [
                /*{
                    path: '',
                    component: UserInfo,
                    name: 'userInfo'
                },*/
                {
                    path: 'owned',
                    component: Owned,
                    name: 'owned'
                },
                {
                    path: 'rated',
                    component: Rated,
                    name: 'rated'
                },
                {
                    path: 'friends',
                    component: Friendslist,
                    name: 'friends'
                },
                {
                  path: 'wishlist',
                  component: Wishlist,
                  name: 'wishlist'
                },
                {
                    path: 'settings',
                    component: Settings,
                    name: 'settings'
                },
            ],
            beforeEnter (to, from, next) {
                if(localStorage.getItem('token')) {
                    next()
                } else {
                    next('/signin')
                }
            }
        },
        {
          path: '/ranking',
          name: 'ranking',
          component: Ranking,
            beforeEnter (to, from, next) {
                if(localStorage.getItem('token')) {
                    next()
                } else {
                    next('/signin')
                }
            }
        },
        {
          path: '/messages',
          name: 'messages',
          component: Message,
            beforeEnter (to, from, next) {
                if(localStorage.getItem('token')) {
                    next()
                } else {
                    next('/signin')
                }
            }
        },
        {
            path: '/admindashboard',
            name: 'admindashboard',
            component: AdminDashboard,
            beforeEnter (to, from, next) {
                if(localStorage.getItem('token')) {
                    next()
                } else {
                    next('/signin')
                }
            }
        }
    ]
})

const DEFAULT_TITLE = 'RateIt';
router.afterEach((to, from) => {
    document.title = to.meta.title || DEFAULT_TITLE;
});

export default router;

