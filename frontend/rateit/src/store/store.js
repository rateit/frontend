import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import auth from './modules/auth'
import messages from './modules/messages'
import search from './modules/search'
import router from '../router';

Vue.use(Vuex);
Vue.config.devtools = true;

export default new Vuex.Store({
  modules: {
      user,
      auth,
      messages,
      search
  }
});
