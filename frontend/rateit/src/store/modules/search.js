import axios from 'axios';
import auth from './auth';

const state = {
    searchValue: '',
    userSearch: '',
    albumSearch: '',
    artistSearch: '',
    physicalSearch: '',
    advancedSearch: '',

    // pagination
    albumPagination: '',
    artistPagination: '',
    physicalPagination: '',
    advancedPagination: '',
};

const getters = {
    getSearchValue(state) {
      return state.searchValue;
    },

    // w przypadku potrzeby paginacji
    // usunac .cds
    getUserSearch(state) {
        return state.userSearch;
    },

    getAlbumSearch(state) {
        return state.albumSearch.cds;
    },

    getArtistSearch(state) {
        return state.artistSearch.cds;
    },

    getPhysicalSearch(state) {
      return state.physicalSearch.cds;
    },

    getAdvancedSearch(state) {
        return state.advancedSearch.cds;
    },

    getAlbumPagination(state) {
        return state.albumPagination;
    },

    getArtistPagination(state) {
        return state.artistPagination
    }
};

const mutations = {
    setSearchValue(state, search) {
        state.searchValue = search;
    },

    setUserSearch(state, user) {
        state.userSearch = user;
    },

    setAlbumSearch(state, album) {
        state.albumSearch = album;
    },

    setArtistSearch(state, artist) {
        state.artistSearch = artist;
    },

    setPhysicalSearch(state, album) {
      state.physicalSearch = album
    },

    setAdvancedSearch(state, advanced) {
        state.advancedSearch = advanced
    },

    emptyUserSearch(state) {
        state.userSearch = ''
    },

    emptyAlbumSearch(state) {
        state.albumSearch = ''
    },

    emptyArtistSearch(state) {
        state.artistSearch = ''
    },

    emptyAdvancedSearch(state) {
        state.advancedSearch = ''
    },

    emptyPhysicalSearch(state) {
        state.physicalSearch = ''
    },

    setAlbumPagination(state, context) {
        state.albumPagination = context
    },

    setArtistPagination(state, context) {
        state.artistPagination = context
    }
};

const actions = {
    fetchSearchValue({commit}, search) {
        commit('setSearchValue', search)
    },

    fetchUsers({commit}, nickname) {
        const token = auth.state.idToken;
        axios.get(`/api/user-by-nick?nick=${nickname}`, {
            headers: {
                Authorization: token
            }
        })
            .then(resp => {
                commit('setUserSearch', resp.data)
            })
            .catch(e => console.log(e));
    },

    fetchAlbums({commit}, albumObj) {
        const token = auth.state.idToken;
        const totalPages = null;
        axios.get(`/api/cds?name=${albumObj.input}&size=8&page=${albumObj.pagin}`, {
            headers: {
                Authorization: token
            }
        })
            .then(resp => {
                commit('setAlbumSearch', resp.data)
                console.log('Albums vuex')
                console.log(resp.data.context);
                commit('setAlbumPagination', resp.data.context)
                //this.filteredAlbums = resp.data.cds;
                const totalPages = resp.data.context.totalPages;

                // this.emitFiltered();
            })
            .catch(e => console.log(e));
    },

    fetchArtists({commit}, artistObj) {
        const token = auth.state.idToken;
        const page = 1;
        const totalPages = null; // bylo w komponencie
        axios.get(`/api/cds?artist=${artistObj.input}&size=8&page=${artistObj.pagin}`, {
            headers: {
                Authorization: token
            }
        })
            .then(resp => {
                const totalPages = resp.data.context.totalPages;
                commit('setArtistSearch', resp.data)
                commit('setArtistPagination', resp.data.context)
            })
            .catch(e => console.log(e));
    },

    fetchPhysical({commit}, query) {
        const token = auth.state.idToken;
        const page = 1;
        const totalPages = null;

        axios.get(query, {
            headers: {
                Authorization: token
            }
        }).then(resp => {
            console.log(resp);
            commit('setPhysicalSearch', resp.data)
        }).catch(e => console.log(e));
    },

    fetchAdvancedSearch({commit}, query) {
        const token = auth.state.idToken;

        axios.get(query, {
            headers: {
                Authorization: token
            }
        })
            .then(resp => {
                console.log(resp);
                commit('setAdvancedSearch', resp.data)
                // this.filteredAlbums = resp.data.cds;
                // const totalPages = resp.data.context.totalPages;
                // this.totalPages = totalPages;
            })
            .catch(e => console.log(e));
    }
};

export default { state, getters, mutations, actions }
