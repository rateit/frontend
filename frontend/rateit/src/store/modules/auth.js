import axios from 'axios'
import router from '../../router'

const state = {
    user: null,
    userID: null,
    idToken: null
};

const getters = {
    user(state) {
        return state.user;
    },

    getToken(state) {
        return state.idToken;
    },

    getUserId(state) {
        return state.userID;
    }
};

const mutations = {
    storeUser (state, user) {
        state.user = user;
        state.userID = user.id;
        state.idToken = user.authToken;
        // router.replace('/dashboard');
    },

    clearAuthData(state) {
        state.idToken = null;
        state.user = null;
        state.userID = null;
        router.replace('/signin');
    }
};

const actions = {
        login({ commit }, authData) {
            return new Promise((resolve, reject) => {
                axios.post('/login', {
                    username: authData.username,
                    password: authData.password
                })
                    .then(resp => {
                        resolve(resp);
                        console.log(resp);
                        localStorage.setItem('token', resp.data.authToken);
                        localStorage.setItem('userId', resp.data.id);
                        localStorage.setItem('user', resp.data);
                        commit('storeUser', resp.data);
                        setTimeout(() => {
                            if(resp.data.role === 'USER') router.replace('/dashboard')
                            else if(resp.data.role === 'ADMIN') router.replace('/adminDashboard')

                        }, 1500)

                    })
                    .catch(e => {
                        console.log(e);
                        reject(e);
                    });
            })
        },


    tryAutoLogin({commit}) {
        const token = localStorage.getItem('token');
        const userId = localStorage.getItem('userId');
        const user = localStorage.getItem('user');
        if(!token) {
            return;
        }
        commit('storeUser', {
            user: user,
            id: userId,
            authToken: token
        });
        console.log(token, userId, user)

    },

    logout ({commit}) {
        const token = state.idToken;
        axios.post('/api/logout',{},{
            headers: {
                Authorization: token
            }
        }).then(resp => {
            console.log(resp)
            commit('clearAuthData');
            localStorage.removeItem('token');
            localStorage.removeItem('userId');
            localStorage.removeItem('user');
        }).catch(e => console.log(e));
    }
};

export default {
    state, getters, mutations, actions
}
