const state = {
    receiverId: null
};

const getters = {
    getReceiverId(state) {
        return state.receiverId;
    }
};

const mutations = {
    setReceiverId(state, id) {
        state.receiverId = id;
    },

    resetId(state) {
        state.receiverId = null;
    }
};

const actions = {
    receiverMessageId({commit}, id) {
        commit('setReceiverId', id)
    },

    resetState({commit}) {
        commit('resetId')
    }
};

export default {
    state, getters, mutations, actions
}
