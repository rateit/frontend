import axios from 'axios'
import auth from "./auth";

const state = {
    friendObj: ''
};

const getters = {
   getFriendObj(state) {
       return state.friendObj;
   }
};

const mutations = {
    setFriendObj(state, friend) {
        state.friendObj = friend;
    }
};

const actions = {
    fetchFriendData({commit}, paramsId) {
        const token = auth.state.idToken;

        axios.get(`/api/users/${paramsId}`, {
            headers: {
                Authorization: token,
            }
        })
            .then(resp => {
                commit('setFriendObj', resp.data)
            })
            .catch(e => console.log(e));
    }
};

export default  {
    state, getters, mutations, actions
}

