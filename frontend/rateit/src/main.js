import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import vuetify from './plugins/vuetify'
import Vuetify from 'vuetify'
import Vuelidate from 'vuelidate'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import axios from 'axios'

export const eventBus = new Vue();

Vue.use(Vuetify)
Vue.use(Vuelidate)
Vue.config.productionTip = false

axios.defaults.baseURL = 'http://www.rate-it.org:8080';

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')


